import { Component, OnDestroy, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

import { Subscription } from 'rxjs';

import { CheckoutService } from './shared/checkout.service';
import { Order } from '../models/order.model';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit, OnDestroy, OnChanges {
  checkoutSubscription: Subscription;
  steps: string[];
  activeStep: number;
  currentOrder: Order; //setting now shiiping cost
  shippinhCost : number = 0;

  constructor(private checkoutService: CheckoutService) {}
  ngOnChanges(changes:SimpleChanges): void {
// if(changes[]){

// }
  }

  ngOnInit() {
    this.checkoutService.currentMessage.subscribe(message => {
      let observer:any = message;
      console.log('observer');
      console.log(observer);
      console.log('observer');
      this.shippinhCost = observer.shippingCost;
    });
    if(this.checkoutService.getOrderInProgress().shippingCost){
      this.shippinhCost = this.checkoutService.getOrderInProgress().shippingCost;
    }else{

      console.log(this.shippinhCost);
      console.log('orderInProgress cost null in checkout');
    }

    this.steps = ['1. Address', '2. Shipping', '3. Payment', '4. Review'];
    this.activeStep = this.checkoutService.activeStep;
    this.checkoutSubscription = this.checkoutService.stepChanged.subscribe((step: number) => {
      this.activeStep = step;
    });
  }

  ngOnDestroy() {
    this.checkoutSubscription.unsubscribe();
  }
}
