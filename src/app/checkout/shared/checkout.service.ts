import { Injectable, EventEmitter } from '@angular/core';
import { Order } from '../../models/order.model';
import { Customer } from '../../models/customer.model';
import { CartItem } from '../../models/cart-item.model';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class CheckoutService {
  private orderInProgress: Order;
  public orderInProgressChanged: EventEmitter<Order> = new EventEmitter<Order>();
  public stepChanged: EventEmitter<number> = new EventEmitter<number>();
  public activeStep: number;
  private messageSource = new BehaviorSubject({});
  currentMessage = this.messageSource.asObservable();

  constructor() {
    this.orderInProgress = new Order(new Customer());
    this.activeStep = 0;
  }

  public gotoStep(number) {
    this.activeStep = number;
    this.stepChanged.emit(this.activeStep);
  }


  public nextStep() {
    this.activeStep++;
    this.stepChanged.emit(this.activeStep);
  }

  previousStep() {
    this.activeStep--;
    this.stepChanged.emit(this.activeStep);
  }

  public resetSteps() {
    this.activeStep = 0;
  }
  changeMessage(message: Order) {
    this.messageSource.next(message)
  }
  public setCustomer(customer: Customer) {
    this.orderInProgress.customer = customer;
    this.changeMessage(this.orderInProgress);
    this.orderInProgressChanged.emit(this.orderInProgress);
  }

  public setShippingMethod(shippingMethod: string) {
    this.orderInProgress.shippingMethod = shippingMethod;
    this.changeMessage(this.orderInProgress);
    this.orderInProgressChanged.emit(this.orderInProgress);
  }
  public setShippingCost(shippingCost: number) {
    this.orderInProgress.shippingCost = shippingCost;
    this.changeMessage(this.orderInProgress);
    this.orderInProgress.total +=   shippingCost;
    this.orderInProgressChanged.emit(this.orderInProgress);
  }
  public setShippingDate(shippingDate: number) {
    this.orderInProgress.shippingDate = shippingDate;
    this.changeMessage(this.orderInProgress);
    this.orderInProgressChanged.emit(this.orderInProgress);
  }

  public setOrderItems(items: CartItem[]) {
    this.orderInProgress.items = items;
    this.changeMessage(this.orderInProgress);
    this.orderInProgressChanged.emit(this.orderInProgress);
  }
  public getShippingCost() {
    return this.orderInProgress.shippingCost ? this.orderInProgress.shippingCost: 0 ;
  }
  public getOrderInProgress() {
    return this.orderInProgress;
  }

  public setPaymentMethod(paymentMethod: string) {
    this.orderInProgress.paymentMethod = paymentMethod;
    this.changeMessage(this.orderInProgress);
    this.orderInProgressChanged.emit(this.orderInProgress);
  }
}
