import { Component, OnInit, Input, OnChanges } from '@angular/core';

import { CartService } from '../../cart/shared/cart.service';
import { CheckoutService } from '../shared/checkout.service';
import { Order } from '../../models/order.model';

@Component({
  selector: 'app-checkout-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit,OnChanges {
  public cartSubtotal: number;
  @Input() shipping: number = 0;
  public orderTotal: number;

  constructor(private cartService: CartService, private checkoutService: CheckoutService) {}
  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    this.cartSubtotal = this.cartService.getTotal();
    // TODO: shipping, hardcoded for now
    let orderInProgress : Order = this.checkoutService.getOrderInProgress();
    if(    this.shipping = orderInProgress.shippingCost){
      console.log(orderInProgress);
    }else{
      console.log(orderInProgress);
      if(!this.shipping){
        this.shipping = 0;
      }
      console.log(this.shipping);
      console.log('orderInProgress cost null');
    }

    this.orderTotal = this.cartSubtotal + orderInProgress.shippingCost;
  }

  
  ngOnInit() {
    this.cartSubtotal = this.cartService.getTotal();
    // TODO: shipping, hardcoded for now
    let orderInProgress : Order = this.checkoutService.getOrderInProgress();
    if(    this.shipping = orderInProgress.shippingCost){
      console.log(orderInProgress);
    }else{
      console.log(orderInProgress);
      if(!this.shipping){
        this.shipping = 0;
      }
      console.log(this.shipping);
      console.log('orderInProgress cost null');
    }

    this.orderTotal = this.cartSubtotal + orderInProgress.shippingCost;
  }
}
