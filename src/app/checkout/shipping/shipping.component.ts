import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CheckoutService } from '../shared/checkout.service';
import { Customer } from '../../models/customer.model';

@Component({
  selector: 'app-checkout-shipping',
  templateUrl: './shipping.component.html',
  styleUrls: ['./shipping.component.scss']
})
export class ShippingComponent implements OnInit {
  public formShipping: FormGroup;
  public shippingMethods: {method: string, time: string, fee: number, value: string,days : number}[];

  constructor(private checkoutService: CheckoutService) { }

  ngOnInit() {
    this.shippingMethods = [
      {
        method: 'Priority',
        time: '1 - 2 days',
        fee: 3,
        value: 'priority',
        days : 2
      },
      {
        method: 'Economy',
        time: 'up to one week',
        fee: 1.2,
        value: 'economy',
        days : 7
      }
    ];
    this.formShipping = new FormGroup({
      'shippingMethod': new FormControl(this.shippingMethods[1].value, Validators.required)
    });
  }

  public onBack() {
    this.checkoutService.previousStep();
  }

  public onContinue() {
    this.checkoutService.setShippingMethod(this.formShipping.controls.shippingMethod.value);
    let tempShipping = this.shippingMethods.filter(ele=>{
      return this.formShipping.controls.shippingMethod.value.toLocaleLowerCase() === ele.value.toLocaleLowerCase()
    })[0];
    this.checkoutService.setShippingCost(tempShipping.fee);
    this.checkoutService.setShippingDate(tempShipping.days);
    this.checkoutService.nextStep();
  }

}
