import { Injectable } from '@angular/core';

// import common service
import { MainService } from '../../../../infrastructure/api.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InitService {
  constructor(private _apiService: MainService) {}
  loggedUser: string = window.sessionStorage.getItem('userId');

  getAllProducts(): Observable<any> {
    return this._apiService.get(`product/getAll`);
  }
  getProductById(prmsId){
    return this._apiService.get(`product/getAll`, prmsId);
  }
  getProductsByCondition(paramsMap: Map<string,string>){
    return this._apiService.get(`product/highlight/filter`, paramsMap);
  }
  updateProductById(obj): Observable<any> {
    return this._apiService.patch(`product/update/`+obj.id, obj);
  }
  createProduct(obj): Observable<any> {
    return this._apiService.post(`product/new`, obj);
  }
  // postObjArrvalTime(obj): Observable<any> {
  //   return this._apiService.post(`googleApi/getDataWithArrivelTime`, obj);
  // }
  // postObjNearByPlaceMapRequest(obj): Observable<any> {
  //   return this._apiService.post(`googleApi/getNearByPlaces`, obj);
  // }

  // validatePoint(obj): Observable<any> {
  //   return this._apiService.post(`googleApi/validatePoint`, obj);
  // }

  removeProduct(id): Observable<any> {
    return this._apiService.delete(`product/remove/${id}`);
  }
}
