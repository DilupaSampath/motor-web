import { Component, OnDestroy, OnInit } from '@angular/core';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { MessageService } from '../../messages/message.service';
import { ProductService } from '../../products/shared/product.service';
import { ProductsCacheService } from '../../products/shared/products-cache.service';
import { PromoService } from '../shared/promo.service';

import { Product } from '../../models/product.model';
import { Promo } from '../../models/promo.model';
import { InitService } from './services/init/init.service';
import { Router } from '@angular/router';
import { AuthServiceUser } from '../../account/shared/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  private unsubscribe$ = new Subject();
  public products: Product[]=[];
  public productsFeatured: any[]=[];
  public productsNewArrivals: Product[]=[];
  public productsOnSale: Product[]=[];
  public productsBestRated: Product[]=[];
  public promos: Promo[]=[];

  constructor(
    private messageService: MessageService,
    private productsCache: ProductsCacheService,
    private productService: ProductService,
    private promoService: PromoService,
    private _InitService: InitService,
    private router: Router,
    private authService: AuthServiceUser
  ) {}

  ngOnInit() {
    let item = sessionStorage.getItem('userId');
    if(item){
      this.authService.getUserByApiId(item).subscribe(data=>{
        console.log('data.data UUUUUUUUU%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
        console.log(data.data);
        
      });;
    }


    let role = sessionStorage.getItem('role');
    console.log('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
    console.log(role);
    if (!role){
      this.router.navigate(['register-login']);
    }
    let paramMapFeatured = new Map();
    paramMapFeatured.set('is_featured',true);
    let paramMapNewArrival = new Map();
    paramMapNewArrival.set('is_new_arrivals',true);
    let paramMapBestRated = new Map();
    paramMapBestRated.set('is_best_rated',true);
    let paramMapOnSale = new Map();
    paramMapOnSale.set('is_on_sale',true);
    // this.productService
    // .getFeaturedProducts()
    // .pipe(takeUntil(this.unsubscribe$))
    // .subscribe(
    //   (products) => {
    //     // this.productsFeatured = products;
    //     console.log(this.productsFeatured);
    //     console.log('this.productsFeatured%%%%%%%%%%%%%%%%%###');
    //   },
    //   (err) => console.error(err)
    // );

    this._InitService
      .getAllProducts()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((data) => {
        if(data.status && data.data.length>0){
          this.products = <Product[]>data.data;
        }

        console.log(this.products);
        console.log(' All   ***this.products');
      });

    this._InitService
      .getProductsByCondition(paramMapFeatured)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (data) => {
          console.log(data);
          if(data.status && data.data.length>0){
            data.data.forEach(element => {
              console.log(element);
              element['imageFeaturedUrl']= element.image_urls[0];            
            });
            this.productsFeatured = data.data;
          }
          console.log(this.productsFeatured);
          console.log(' productsFeatured   ***this.products');
        },
        (err) => console.error(err)
      );

    this._InitService
      .getProductsByCondition(paramMapNewArrival)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (data) => {
          console.log(data);
          if(data.status && data.data.length>0){
            data.data.forEach(element => {
              console.log(element);
              element['imageFeaturedUrl']= element.image_urls[0];            
            });
            this.productsNewArrivals = data.data;
          }
          console.log(this.productsNewArrivals);
          console.log(' productsNewArrivals   ***this.products');
        },
        (err) => console.error(err)
      );

    this._InitService
      .getProductsByCondition(paramMapBestRated)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (data) => {
          console.log(data);
          if(data.status && data.data.length>0){
            data.data.forEach(element => {
              console.log(element);
              element['imageFeaturedUrl']= element.image_urls[0];            
            });
            this.productsBestRated = data.data;
          }
          console.log(this.productsBestRated);
          console.log(' productsBestRated   ***this.products');
        },
        (err) => console.error(err)
      );

    this._InitService
      .getProductsByCondition(paramMapOnSale)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (data) => {
          console.log(data);
          if(data.status && data.data.length>0){
            data.data.forEach(element => {
              console.log(element);
              element['imageFeaturedUrl']= element.image_urls[0];            
            });
            this.productsOnSale = data.data;
          }
          console.log(this.productsOnSale);
          console.log(' productsOnSale   ***this.products');
         },
        (err) => console.error(err)
      );

    this.promoService
      .getPromos()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((promos) => {
        this.promos = promos;
      });
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
