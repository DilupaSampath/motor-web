import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Subscription } from 'rxjs';

import { AuthServiceUser } from '../../account/shared/auth.service';
import { OffcanvasService } from '../shared/offcanvas.service';

import { User } from '../../models/user.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  private authSubscription: Subscription;
  public user: any;
  public showSearch;

  constructor(
    private authService: AuthServiceUser,
    private router: Router,
    private offcanvasService: OffcanvasService
  ) {}

  ngOnInit() {
    this.authService.getUserByApiId(sessionStorage.getItem('userId')).subscribe((user) => {
      if (user) {
        this.user = user.data;
      }
    });
  }

  public onLogOut(e: Event) {
    this.authService.signOut();
    this.router.navigate(['/register-login']);
    e.preventDefault();
  }

  public onMenuToggle(e: Event) {
    this.offcanvasService.openOffcanvasNavigation();
    e.preventDefault();
  }

  ngOnDestroy() {
    // this.authSubscription.unsubscribe();
  }
}
