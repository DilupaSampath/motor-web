import { Component, OnDestroy, OnInit } from '@angular/core';

import { Subscription } from 'rxjs';

import { AuthServiceUser } from '../../../account/shared/auth.service';

import { User } from '../../../models/user.model';

@Component({
  selector: 'app-navigation-main',
  templateUrl: './navigation-main.component.html',
  styleUrls: ['./navigation-main.component.scss']
})
export class NavigationMainComponent implements OnInit, OnDestroy {
  public user: any;
  private authSubscription: Subscription;

  constructor(public authService: AuthServiceUser) {}

  ngOnInit() {
    this.authService.getUserByApiId(sessionStorage.getItem('userId')).subscribe((user) => {
      if (user) {
        this.user = user.data;
      }
    });
  }

  ngOnDestroy() {
    // this.authSubscription.unsubscribe();
  }
}
