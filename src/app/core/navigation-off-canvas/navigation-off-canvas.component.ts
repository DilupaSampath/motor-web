import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Subscription } from 'rxjs';

import { AuthServiceUser } from '../../account/shared/auth.service';
import { OffcanvasService } from '../shared/offcanvas.service';

import { User } from '../../models/user.model';

@Component({
  selector: 'app-navigation-off-canvas',
  templateUrl: './navigation-off-canvas.component.html',
  styleUrls: ['./navigation-off-canvas.component.scss']
})
export class NavigationOffCanvasComponent implements OnInit, OnDestroy {
  private authSubscription: Subscription;
  public user: any;
  isAdmin = false;
  constructor(
    public offcanvasService: OffcanvasService,
    public authService: AuthServiceUser,
    private router: Router
  ) { }

  ngOnInit() {
    if (this.authService.getUser()) {
      this.user = this.authService.getUser();
      
      let role = sessionStorage.getItem('role');
      console.log('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
      console.log(role);
      if (role === 'admin') {
        this.isAdmin = true;
      }
    }
    else {
  this.isAdmin = false;
    }
}

  public onLogout(e: Event) {
  this.offcanvasService.closeOffcanvasNavigation();
  this.authService.signOut();
  this.router.navigate(['/register-login']);
  e.preventDefault();
}

  public onNavigationClick() {
  this.offcanvasService.closeOffcanvasNavigation();
}

ngOnDestroy() {
  // this.authSubscription.unsubscribe();
}
}
