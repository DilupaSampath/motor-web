import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'filterUser'
})
export class FilterUserPipe implements PipeTransform {
  transform(items: any[], searchText: string): any[] {
    if (!items) return [];
    if (!searchText) return items;
    searchText = searchText.toLowerCase();
    return items.filter(it => {
      // 
      // console.log(k.toString().toLowerCase());})
      return (it._id && it._id.toString().toLowerCase().includes(searchText.toString().toLowerCase())) || (it.email && it.email.toString().toLowerCase().includes(searchText.toString().toLowerCase())) || (it.first_name && it.first_name.toString().toLowerCase().includes(searchText.toString().toLowerCase())) || (it.last_name && it.last_name.toString().toLowerCase().includes(searchText.toString().toLowerCase()));
    });
  }

}