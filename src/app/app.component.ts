import { Component, OnChanges, OnInit } from '@angular/core';
import { OffcanvasService } from './core/shared/offcanvas.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnChanges, OnInit {
  public products: any;
  isActiveAppheader = true;

  constructor(public offcanvasService: OffcanvasService,
    public router: Router,
    private route: ActivatedRoute) { }
  ngOnChanges() {
  }
  ngOnInit() {
    this.route.data.subscribe(data => console.log(data));
  }

}
