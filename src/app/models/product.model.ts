export class Product {
  public imageFeaturedUrl?;

  constructor(
    public id: number = 1,
    public date: string = new Date().toISOString().split('T')[0],
    public name: string = '',
    public description: string = '',
    public actual_price: number = 0,
    public original_price: number = 0,
    public reduction: number = 0,
    public image_urls: string[] = [],
    public image_resize_urls: string[] = [],
    public image_refs: string[] = [],
    public categories: {} = {},
    public ratings: {} = {},
    public currentRating: number = 0,
    public stock_count: number = 1,
    public sale: boolean = false
  ) {}
}
