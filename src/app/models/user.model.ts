import { Order } from './order.model';

export interface Roles {
  admin: boolean;
}

export class User {
  public email: string;
  public photoURL?: string;
  public role?: any;
  public firstName?: string;
  public lastName?: string;
  public password?: string;
  public orders?: object;
  public confirmPassword?: string;
  public uid?: string;

  constructor(authData) {
    this.email = authData.email ? authData.email : '';
    this.firstName = authData.firstName ? authData.firstName : '';
    this.lastName = authData.lastName ? authData.lastName : '';
    this.role = 'user';
  }
  createNewUser(password){
    this.password = password;
  }
  createUser(data){
    this.email = data.email;
    this.firstName = data.firstName ? data.firstName : '';
    this.lastName = data.lastName ? data.lastName : '';
    this.role = data.role ? data.role : 'user';
    this.uid = data.uid ? data.uid : '';
    this.orders = data.orders ? data.orders : [];
    this.photoURL = data.photoURL ? data.photoURL : '';
    return 
  }
}
