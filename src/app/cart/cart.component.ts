import { Component, OnInit, OnDestroy } from '@angular/core';

import { Subscription } from 'rxjs';

import { CartService } from './shared/cart.service';
import { CartItem } from '../models/cart-item.model';
import { ActivatedRoute, Params } from '@angular/router';
import { InitService } from './shared/init/init.service';
import swal from 'sweetalert2';
import { MessageService } from '../messages/message.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit, OnDestroy {
  private cartSubscription: Subscription;
  public items: CartItem[];
  public total: number;
  private sub: any;
  curreniCartId = null;
  constructor(private cartService: CartService, public route: ActivatedRoute, private _InitService: InitService,
    private log: MessageService) { }

  ngOnInit() {

    this.sub = this.route.params.subscribe(params => {
      let id = params['id'];
      // if we have an id, we're in edit mode
      console.log('id ************%%%%%%%%%%%%%%%% first');
      console.log(id);
      console.log(params);
      console.log('id ************%%%%%%%%%%%%%%%% first');
      if (id) {

        let timerInterval
        swal.fire({
          imageUrl: 'https://media.giphy.com/media/1svYRwGoEYwd1cor99/giphy.gif',
          imageWidth: 100,
          imageHeight: 80,
          title: `Please wait i'm loading your existing cart....`,
          timer: 2000,
          timerProgressBar: true,
          onBeforeOpen: () => {
            swal.showLoading()
            timerInterval = setInterval(() => {
              const content = swal.getContent()
              if (content) {
                const b = content.querySelector('b') as HTMLCanvasElement;
                if (b) {
                  b.textContent = swal.getTimerLeft().toString();
                }
              }
            }, 100)
          },
          onClose: () => {
            clearInterval(timerInterval)
          }
        }).then((result) => {
          /* Read more about handling dismissals below */
          if (result.dismiss === swal.DismissReason.timer) {
            // console.log('I was closed by the timer')
          }
        });

        this.cartService.setCartId(id);
        this._InitService.getCartById(id).subscribe(data => {
          if (data.data) {
            let cartData: any[] = data.data.cart_items;
            this.cartService.loadExistingCart(cartData, id);

            console.log('id ************%%%%%%%%%%%%%%%%OOKKKKK');
            console.log(id);
            console.log(data);
            console.log(data.data);
          }
        });
      } else {
        console.log('id ************%%%%%%%%%%%%%%%% not found');
      }


    });
    this.items = this.cartService.getItems();
    this.total = this.cartService.getTotal();
    this.cartSubscription = this.cartService.itemsChanged.subscribe(
      (items: CartItem[]) => {
        this.items = items;
        this.total = this.cartService.getTotal();
      }
    );
    console.log(this.items);
  }

  public onClearCart(event) {
    event.preventDefault();
    event.stopPropagation();
    this.cartService.clearCart();
  }

  public onRemoveItem(event, item: CartItem) {
    event.preventDefault();
    event.stopPropagation();
    this.cartService.removeItem(item);
  }

  public increaseAmount(item: CartItem) {
    let temp_amountOld = JSON.parse(JSON.stringify(item.amount));
    if(item.product.stock_count >= item.amount + 1){
      this.cartService.updateItemAmount(item, item.amount + 1);
    }else{
      item.amount = temp_amountOld;
      this.log.addError('Product is out of stock. You can add only up to qty: '+item.amount);
    }
  }

  public decreaseAmount(item: CartItem) {
    const newAmount = item.amount === 1 ? 1 : item.amount - 1;
    this.cartService.updateItemAmount(item, newAmount);
  }

  public checkAmount(item: CartItem) {
    this.cartService.updateItemAmount(
      item,
      item.amount < 1 || !item.amount || isNaN(item.amount) ? 1 : item.amount
    );
  }

  ngOnDestroy() {
    this.cartSubscription.unsubscribe();
  }
}
