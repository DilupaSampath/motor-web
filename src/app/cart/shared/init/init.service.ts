import { Injectable } from '@angular/core';

// import common service
import { MainService } from '../../../infrastructure/api.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InitService {
  constructor(private _apiService: MainService) { }
  loggedUser: string = window.sessionStorage.getItem('userId');

  getAllCarts(): Observable<any> {
    return this._apiService.get(`cart/getAll`);
  }
  getCartById(prmsId) {
    return this._apiService.get(`cart/get/` + prmsId);
  }
  getCartsByCondition(paramsMap: Map<string, string>) {
    return this._apiService.get(`cart/getAll`, paramsMap);
  }
  updateCartById(obj,id): Observable<any> {
    return this._apiService.patch(`cart/update/` + id, obj);
  }
  createCart(obj): Observable<any> {
    return this._apiService.post(`cart/new`, obj);
  }
  removeCart(id): Observable<any> {
    return this._apiService.delete(`cart/remove/${id}`);
  }
}
