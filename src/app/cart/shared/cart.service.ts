import { EventEmitter, Injectable } from '@angular/core';
import { Product } from '../../models/product.model';
import { CartItem } from '../../models/cart-item.model';
import { MessageService } from '../../messages/message.service';
import { InitService } from './init/init.service';
import { CheckoutService } from '../../checkout/shared/checkout.service';

@Injectable()
export class CartService {
  // Init and generate some fixtures
  private cartItems: CartItem[];
  public itemsChanged: EventEmitter<CartItem[]> = new EventEmitter<CartItem[]>();
  private currentCartId = null;
  constructor(private messageService: MessageService, private _InitService: InitService, private checkoutService: CheckoutService) {
    this.cartItems = [];
  }

  public getCurrentCartId() {
    return this.currentCartId;
  }
  public getItems() {
    return this.cartItems.slice();
  }

  // Get Product ids out of CartItem[] in a new array
  private getItemIds() {
    return this.getItems().map(cartItem => cartItem.product.id);
  }

  public addItem(item: CartItem) {
    let currentCart = this.getIdFromSessionStroage();

    // If item is already in cart, add to the amount, otherwise push item into cart
    if (this.getItemIds().includes(item.product.id)) {
      this.cartItems.forEach(function (cartItem) {
        if (cartItem.product.id === item.product.id) {
          cartItem.amount += item.amount;
        }
      });
      if (currentCart) {
        this.updateCart(this.cartItems);
      } else {
        this.createCartWithAll();
      }
    } else {
      this.cartItems.push(item);
      this.createCart(item);
    }
  }
  getIdFromSessionStroage() {
    let id = this.currentCartId = sessionStorage.getItem('cartId');
    if (!this.currentCartId) {
      id = this.currentCartId = sessionStorage.getItem('cartId');
    } else {
      id = this.currentCartId;
    }
    return id ? id : null;
  }
  updateCart(item, type?) {
    if (!this.currentCartId) {
      this.currentCartId = this.getIdFromSessionStroage();
    }

    if (this.currentCartId) {
      this._InitService.updateCartById({ cart_items: this.cartItems }, this.currentCartId).subscribe(catrData => {
        if (catrData.data) {
          this.currentCartId = catrData.data._id;
          sessionStorage.setItem('cartId', catrData.data._id);
          this.itemsChanged.emit(this.cartItems.slice());
          console.log(catrData);
          console.log(catrData);
          if (type =='deleteItem') {
            this.messageService.add('Deleted from cart: ' + item.product.name);
          } else if(type =='clear') {
            this.messageService.add('Cleared cart');
          } else {

            this.messageService.add('Amount in cart changed for: ' + item.product.name);
            this.messageService.add('Added to cart: ' + item.product.name);
          }
        }
      }, (error) => {
        console.log(error);
      });
    } else {
      this.messageService.addError('Cannot found your cart..!');
    }
  }
  createCartWithAll() {
    this._InitService.createCart({ cart_items: this.cartItems }).subscribe(catrData => {
      if (catrData.data.cart_items) {
        sessionStorage.setItem('cartId', catrData.data._id);
        let tempCartItems: CartItem[] = [];
        catrData.data.cart_items.forEach(element => {
          let tempCart = new CartItem(element.product, element.amount);
          tempCartItems.push(tempCart);
        });
        this.cartItems = tempCartItems;
        this.itemsChanged.emit(this.cartItems.slice());
        console.log(catrData);
        console.log(catrData);
        this.messageService.add('Your last cart is loaded');
      }
    }, (error) => {

    });
  }
  removeCart() {
    if (!this.currentCartId) {
      this.currentCartId = this.getIdFromSessionStroage();
    }
    this._InitService.removeCart(this.currentCartId).subscribe(catrData => {
      if (catrData.data) {
        sessionStorage.removeItem('cartId');
        this.cartItems = [];
        this.itemsChanged.emit(this.cartItems.slice());
        console.log(catrData);
        console.log(catrData);
        this.messageService.add('Cleared cart');
      }
    }, (error) => {

    });
  }
  createCart(item) {
    this._InitService.createCart({ cart_items: this.cartItems }).subscribe(catrData => {
      if (catrData.data.cart_items) {
        sessionStorage.setItem('cartId', catrData.data._id);
        let tempCartItems: CartItem[] = [];
        catrData.data.cart_items.forEach(element => {
          let tempCart = new CartItem(element.product, element.amount);
          tempCartItems.push(tempCart);
        });
        this.cartItems = tempCartItems;
        this.itemsChanged.emit(this.cartItems.slice());
        console.log(catrData);
        console.log(catrData);
        this.messageService.add('Amount in cart changed for: ' + item.product.name);
        this.messageService.add('Added to cart: ' + item.product.name);
      }
    }, (error) => {

    });
  }
  public addItems(items: CartItem[]) {
    items.forEach((cartItem) => {
      this.addItem(cartItem);
    });
  }

  public removeItem(item: CartItem) {
    const indexToRemove = this.cartItems.findIndex(element => element === item);
    this.cartItems.splice(indexToRemove, 1);
    this.itemsChanged.emit(this.cartItems.slice());
    this.updateCart(item);
  }

  public setCartId(id) {
    if (id) {
      sessionStorage.setItem('cartId', id);
    }
  }
  public updateItemAmount(item: CartItem, newAmount: number) {
    this.cartItems.forEach((cartItem) => {
      if (cartItem.product.id === item.product.id) {
        cartItem.amount = newAmount;
      }
    });
    this.updateCart(item);
    this.itemsChanged.emit(this.cartItems.slice());
    // this.messageService.add('Updated amount for: ' + item.product.name);
  }

  public clearCart() {
    this.cartItems = [];
    this.itemsChanged.emit(this.cartItems.slice());
    this.updateCart(null,'clear');
    // this.removeCart();
  }
  public loadExistingCart(cartGetResponce: any[], id) {
    sessionStorage.setItem('cartId', id);
    this.currentCartId = id;
    this.cartItems = [];
    let tempCartItems: CartItem[] = [];
    cartGetResponce.forEach(element => {
      let tempCart = new CartItem(element.product, element.amount);
      tempCartItems.push(tempCart);
    });
    this.cartItems = tempCartItems;
    // this.getTotal();
    this.itemsChanged.emit(this.cartItems.slice());
  }

  public getTotal() {
    let total = 0;
    this.cartItems.forEach((cartItem) => {
      total += cartItem.amount * cartItem.product.actual_price;
    });
    return total;
  }

}
