import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {
  transform(items: any[], searchText: string): any[] {
    if (!items) return [];
    if (!searchText) return items;
    searchText = searchText.toLowerCase();
    return items.filter(it => {
      // 
      // console.log(k.toString().toLowerCase());})
      return it.date.toString().toLowerCase().includes(searchText.toString().toLowerCase()) || it.id.toString().toLowerCase().includes(searchText.toString().toLowerCase()) || it.name.toString().toLowerCase().includes(searchText.toString().toLowerCase()) || it.actual_price.toString().toLowerCase().includes(searchText.toString().toLowerCase()) || it.original_price.toString().toLowerCase().includes(searchText.toString().toLowerCase());
    });
  }

}