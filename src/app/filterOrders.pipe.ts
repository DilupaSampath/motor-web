import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'filterOrders'
})
export class FilterOrderPipe implements PipeTransform {
  transform(items: any[], searchText: string): any[] {
    if (!items) return [];
    if (!searchText) return items;
    searchText = searchText.toLowerCase();
    return items.filter(it => {
      // 
      // console.log(k.toString().toLowerCase());})
      return (it._id && it._id.toString().toLowerCase().includes(searchText.toString().toLowerCase())) || it.order_id && it.order_id.toString().toLowerCase().includes(searchText.toString().toLowerCase()) || (it.recipient_name && it.recipient_name.toString().toLowerCase().includes(searchText.toString().toLowerCase())) || (it.contact_number && it.contact_number.toString().toLowerCase().includes(searchText.toString().toLowerCase())) || (it.status && it.status.toString().toLowerCase().includes(searchText.toString().toLowerCase()));
    });
  }

}