import { Component, OnDestroy, OnInit } from '@angular/core';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { AuthServiceUser } from '../../account/shared/auth.service';
import { PagerService } from '../../pager/pager.service';
import { ProductsCacheService } from '../shared/products-cache.service';
import { ProductService } from '../shared/product.service';
import { UiService } from '../shared/ui.service';
import { SortPipe } from '../shared/sort.pipe';

import { Product } from '../../models/product.model';
import { User } from '../../models/user.model';
import { InitService } from '../services/init/init.service';

@Component({
  selector: 'app-products',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss']
})
export class ProductsListComponent implements OnInit, OnDestroy {
  unsubscribe$ = new Subject();
  products: any[];
  productsPaged: Product[];
  pager: any = {};
  user: any;
  productsLoading: boolean;
  currentPagingPage: number;

  constructor(
    private productService: ProductService,
    private productsCacheService: ProductsCacheService,
    private pagerService: PagerService,
    private sortPipe: SortPipe,
    private authService: AuthServiceUser,
    public uiService: UiService,
    private _InitService: InitService
  ) {}

  ngOnInit() {
    this.authService.getUserByApiId(sessionStorage.getItem('userId')).subscribe((user) => {
      if (user) {
        this.user = user.data;
      }
    });
    this.uiService.currentPagingPage$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((page) => {
        this.currentPagingPage = page;
      });
    this.getProducts();
  }

  getProducts() {
    this.productsLoading = true;
    this._InitService
      .getAllProducts()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((data) => {
        this.products = <any[]>data.data;
        console.log(data);
        this.productsLoading = false;
//         this.products.forEach(element=>{
// e
//         });
      // this.products[0].imageURLs = ['https://sc02.alicdn.com/kf/Hc13087e3a7cd40f1a1beae8ced90e7c2D.jpg'];
      // this.products[0].name = 'motorcycle scooter stand';

      // this.products[1].imageURLs = ['https://sc02.alicdn.com/kf/H673ef1f7ad45443ea4b889bdaf9cf228T.jpg'];
      // this.products[1].name = 'aluminum die casting parts';

      // this.products[2].imageURLs = ['https://sc01.alicdn.com/kf/Hbf3282b4837e43e08a2fc862a3bef2c42.png'];
      // this.products[0].name = 'customize brass';

      // this.products[3].imageURLs = ['https://sc02.alicdn.com/kf/H98c34c06fc63471cba648c6beb7b4a73T.jpg'];
      // this.products[0].name = 'CNC precision';

      // this.products[4].imageURLs = ['https://sc02.alicdn.com/kf/U5bd6945dec0a4d57a5c9813a21e2f8a0D.jpg'];
      // this.products[0].name = 'Stamping Hyundai Atos';

      // this.products[5].imageURLs = ['https://sc02.alicdn.com/kf/Haa1983b9135d4dab81b89b2c6ec24a291.jpg'];
      // this.products[0].name = 'Factory CNC machined anodized aluminum';

      // this.products[6].imageURLs = ['https://sc01.alicdn.com/kf/Hac222b1f51d943ca829a1365379f9514Y.jpg'];
      // this.products[0].name = 'Rapid Prototyping Cnc turning Brass';

      // this.products[7].imageURLs = ['https://sc01.alicdn.com/kf/H07f400831ca7468da06c58e795a1cd18r.jpg'];
      // this.products[0].name = 'DC CG200 CG250 1LED CDI UNIT';

      // this.products[8].imageURLs = ['https://sc02.alicdn.com/kf/Hc2d4199d7c6e45659c483fcbaeab386bQ.jpg'];
      // this.products[0].name = 'cargo motor water cold engine';

      // this.products[9].imageURLs = ['https://sc02.alicdn.com/kf/H42018e95ac6b4e0b98e0fabb4271cd490.jpg'];
      // this.products[0].name = 'Injection Molding ABS Plastic';

      // this.products[10].imageURLs = ['https://sc01.alicdn.com/kf/H281a53b9b57b4c1c808f87299dc25d3fq.jpg'];
      // this.products[0].name = 'High Precision CNC Brass';

      // this.products[11].imageURLs = ['https://sc02.alicdn.com/kf/H86747738391d46e68930846bf4ce842bY.jpg'];
      // this.products[0].name = 'motorcycle scooter stand';

      // this.products[12].imageURLs = ['https://sc01.alicdn.com/kf/HTB1U8PLKbGYBuNjy0Foq6AiBFXao.jpg'];
      // this.products[0].name = 'motorcycle scooter stand';

        this.setPage(this.currentPagingPage);
        this.productsLoading = false;
      });
  }

  onDisplayModeChange(mode: string, e: Event) {
    this.uiService.displayMode$.next(mode);
    e.preventDefault();
  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }
    this.pager = this.pagerService.getPager(this.products.length, page, 8);
    this.productsPaged = this.products.slice(
      this.pager.startIndex,
      this.pager.endIndex + 1
    );
    this.uiService.currentPagingPage$.next(page);
  }

  onSort(sortBy: string) {
    this.sortPipe.transform(
      this.products,
      sortBy.replace(':reverse', ''),
      sortBy.endsWith(':reverse')
    );
    this.uiService.sorting$.next(sortBy);
    this.setPage(1);
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
