import { Injectable } from '@angular/core';

// import common service
import { MainService } from '../../../../infrastructure/api.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InitService {
  constructor(private _apiService: MainService) {}
  loggedUser: string = window.sessionStorage.getItem('userId');

  removeFile(obj): Observable<any> {
    return this._apiService.post(`images/remove`, obj);
  }
  getAllProducts(): Observable<any> {
    return this._apiService.get(`product/getAll`);
  }
  getAllUsers(): Observable<any> {
    return this._apiService.get(`user/getAll`);
  }
  getAllOrders(): Observable<any> {
    return this._apiService.get(`orders/getAll/admin`);
  }
  getAllHighlightProducts(): Observable<any> {
    return this._apiService.get(`product/highlight/filter/getAll`);
  }
  getProductById(prmsId){
    return this._apiService.get(`product/getAll`, prmsId);
  }
  
  getProductsByCondition(paramsMap: Map<string,string>){
    return this._apiService.get(`product/getAll`, paramsMap);
  }
  updateProductById(obj): Observable<any> {
    return this._apiService.patch(`product/update/`+obj.id, obj);
  }
  updateHighlightProductById(id,obj): Observable<any> {
    return this._apiService.patch(`product/highlight/update/`+id, obj);
  }
  createProduct(obj): Observable<any> {
    return this._apiService.post(`product/new`, obj);
  }
  createReport(obj): Observable<any> {
    return this._apiService.post(`product/get/report`, obj);
  }
  createReportOrder(obj): Observable<any> {
    return this._apiService.post(`order/get/report`, obj);
  }
  createReportUser(obj): Observable<any> {
    return this._apiService.post(`user/get/report`, obj);
  }
  createUserWiseOrderReport(obj): Observable<any> {
    return this._apiService.post(`order/report/by/user`, obj);
  }
  userCreate(obj): Observable<any> {
    return this._apiService.post(`user/new`, obj);
  }
  // postObjArrvalTime(obj): Observable<any> {
  //   return this._apiService.post(`googleApi/getDataWithArrivelTime`, obj);
  // }
  // postObjNearByPlaceMapRequest(obj): Observable<any> {
  //   return this._apiService.post(`googleApi/getNearByPlaces`, obj);
  // }

  // validatePoint(obj): Observable<any> {
  //   return this._apiService.post(`googleApi/validatePoint`, obj);
  // }
  updateUserById(obj,id): Observable<any> {
    delete obj.id;
    return this._apiService.patch(`user/update/`+id, obj);
  }
  updateOrderById(obj,id): Observable<any> {
    delete obj.id;
    return this._apiService.patch(`order/update/`+id, obj);
  }
  removeProduct(id): Observable<any> {
    return this._apiService.delete(`product/remove/${id}`);
  }
  removeUser(id): Observable<any> {
    return this._apiService.delete(`user/delete/${id}`);
  }
  removeOrder(id): Observable<any> {
    return this._apiService.delete(`order/delete/${id}`);
  }
}
