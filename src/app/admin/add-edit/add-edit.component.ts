import { Component, OnDestroy, OnInit, ViewChild, Inject } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable, Subscription, of } from 'rxjs';

import { MessageService } from '../../messages/message.service';
import { FileUploadService } from '../../products/shared/file-upload.service';
import { ProductService } from '../../products/shared/product.service';
import { ProductsCacheService } from '../../products/shared/products-cache.service';
import { Product } from '../../models/product.model';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { InitService } from './services/init/init.service';
import { DOCUMENT } from '@angular/common';
import { FileUploader } from 'ng2-file-upload';
import { environment } from '../../../environments/environment.prod';
declare let $: any;
// we send and receive categories as {key:true},
// but for the input field we need
// a product with categories of type string
const URL = environment.api_url + 'api/upload';

import swal from 'sweetalert2';
import { AuthServiceUser } from '../../account/shared/auth.service';

export class DomainProduct extends Product {
  categories: string;
}

@Component({
  selector: 'app-add-edit',
  templateUrl: './add-edit.component.html',
  styleUrls: ['./add-edit.component.scss']
})
export class AddEditComponent implements OnInit, OnDestroy {
  private productSubscription: Subscription;
  private formSubscription: Subscription;
  @ViewChild('photos', { static: true }) photos;
  public productForm: FormGroup;
  public productReportForm: FormGroup;
  public orderReportForm: FormGroup;
  public userReportForm: FormGroup;
  public product: DomainProduct;
  public mode: 'edit' | 'add';
  public id;
  public percentage: Observable<number>;
  public allProducts: any[];
  public highlightProducts: any[];

  _isDisabled = true;
  activeTab = 0;
  warning = false;
  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: 'auto',
    minHeight: '0',
    maxHeight: 'auto',
    width: 'auto',
    minWidth: '0',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter text here...',
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '',
    fonts: [
      { class: 'arial', name: 'Arial' },
      { class: 'times-new-roman', name: 'Times New Roman' },
      { class: 'calibri', name: 'Calibri' },
      { class: 'comic-sans-ms', name: 'Comic Sans MS' }
    ],
    customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadUrl: 'v1/image',
    uploadWithCredentials: false,
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [
      ['bold', 'italic'],
      ['fontSize']
    ]
  };
  customId: number;
  searchText: any;
  searchTextUser: any;
  searchTextOrder: any;
  imageUrls = [];
  isUploadDisable = true;
  isEditDisable = true;
  allUsers: any[] = [];
  allOrders: any[] = [];
  user: any;
  public registerForm: FormGroup;
  public editForm: FormGroup;
  public uploader: FileUploader = new FileUploader({ url: URL, itemAlias: 'photo' });
  userRole = 'user';
  fname = '';
  lname = '';
  editEmail = '';
  currentEditRole = 'user';
  currentUser: any;
  isActiveEditForm = false;
  constructor(
    private router: Router,
    public route: ActivatedRoute,
    private productService: ProductService,
    public fileUploadService: FileUploadService,
    private productsCacheService: ProductsCacheService,
    private log: MessageService,
    private _InitService: InitService,
    private messageService: MessageService,
    private authService: AuthServiceUser,
    @Inject(DOCUMENT) private document
  ) { }

  ngOnInit(): void {
    // this.getAllHighlightProducts()
    this.initLoginForm();
    this.authService.getUserByApiId(sessionStorage.getItem('userId')).subscribe(data2 => {
      console.log('not getting user get this');
      console.log(data2.data);
      this.authService.setUser(data2.data);
      this.user = this.authService.getUser();
    }, (errorOut: any) => {
    });
    this.getAllUsers();
    this.getAllOrders();
    this.getAllProducts();
    this.setProduct();

    this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
    this.uploader.onErrorItem = (item, response, status, headers) => {
      this.log.addError('Fail to uploaded this file..!');
    };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      //  console.log('ImageUpload:uploaded:', item, status, response);

      console.log(JSON.parse(response).file_path);
      //  console.log(item);
      //  alert('File uploaded successfully');
      this.product.image_urls.push(JSON.parse(response).file_path);
      this.product.image_resize_urls.push(JSON.parse(response).file_path);
      this.imageUrls.push(JSON.parse(response).file_path);
      this.messageService.add('File uploaded successfully..!!');
      console.log(this.product.image_urls);
      console.log(this.imageUrls);
      this.warning = true;
    };

  }
  private initLoginForm() {
    this.registerForm = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email])
    });
  }
  private initEditForm() {
    this.editForm = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email])
    });
  }
  onClickEditUser(user) {
    this.isActiveEditForm = true;
    this.currentUser = user;
    this.fname = user.first_name ? user.first_name : '';
    this.lname = user.last_name ? user.last_name : '';
    this.editEmail = user.email ? user.email : '';
    this.currentEditRole = user.role ? user.role : '';
  }
  onEditUserCancel() {
    this.isActiveEditForm = false;
    this.currentUser = null;
    this.fname = '';
    this.lname = '';
    this.editEmail = '';
    this.currentEditRole = 'user';
  }
  onEditUser() {
    if (this.fname && this.lname && this.editEmail && this.currentUser && this.currentUser._id) {
      let obj = {
        "first_name": this.fname,
        "last_name": this.lname,
        "email": this.editEmail,
        "role": this.currentEditRole
      }
      this._InitService.updateUserById(obj, this.currentUser._id).subscribe(responce => {

        if (responce.data) {
          swal.fire({
            position: 'top-end',
            icon: 'success',
            title: "User updated successfully",
            showConfirmButton: false,
            timer: 3500
          });
          this.isActiveEditForm = false;
          this.getAllUsers();
        } else {
          swal.fire({
            icon: 'error',
            title: 'Oops...! Fail to update this user',
            // text: 'This is '
          });
        }
      }, (error) => {
        swal.fire({
          icon: 'error',
          title: 'Oops...! Fail to update this user',
          // text: 'This is '
        });
      });
    }
  }
  onEditUserConfirm(user) {
    this.isActiveEditForm = true;
    swal.fire({
      title: 'Are you sure?',
      text: "Do you want to save this changes?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, please..!'
    }).then((result) => {
      if (result.value) {
        this.isActiveEditForm = true;
        this.onEditUser();
      }
    })
  }
  changeValue(event) {
    console.log(event);
    this.currentEditRole = event.target.value;
  }
  checkValue(event: any) {
    console.log(event);
    this.userRole = event;
  }
  createUser() {
    console.log(this.registerForm.get('email'));
    console.log(this.registerForm.value);
    console.log(this.registerForm.value.email);
    this._InitService.userCreate({ auto_password: true, role: this.userRole, email: this.registerForm.value.email })
      .subscribe(data => {

        if (data.data) {
          if (data.data.isAlreadyExist) {
            swal.fire({
              icon: 'error',
              title: 'Oops...! User already exist..!',
              text: 'I think you have recheck'
            })
          } else {
            let timerInterval
            swal.fire({
              title: `Please wait i'm processing...`,
              timer: 2000,
              timerProgressBar: true,
              onBeforeOpen: () => {
                swal.showLoading()
                timerInterval = setInterval(() => {
                  const content = swal.getContent()
                  if (content) {
                    const b = content.querySelector('b') as HTMLCanvasElement;
                    if (b) {
                      b.textContent = swal.getTimerLeft().toString();
                    }
                  }
                }, 100)
              },
              onClose: () => {
                clearInterval(timerInterval)
              }
            }).then((result) => {
              /* Read more about handling dismissals below */
              if (result.dismiss === swal.DismissReason.timer) {
                swal.fire({
                  position: 'top-end',
                  icon: 'success',
                  title: "User created completed. Password will send to corresponding user's email address",
                  showConfirmButton: false,
                  timer: 3500
                });
                this.registerForm.setValue({
                  email: ''
                });
                this.userRole = 'user';
                this.getAllUsers();
              }
            });
          }

        } else {

        }
      });

  }
  deleteOrder(id) {

    swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.deleteOrderWithConfirm(id);
      }
    })
  }
  deleteUser(id, email) {
    if (this.user.email === email) {
      // this.messageService.addError("Ooops...! ");
      swal.fire({
        icon: 'error',
        title: 'Oops...! This is you..',
        text: 'I think you have recheck your action'
      })
    } else {
      swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
          this.deleteUserWithConfirm(id, email);
        }
      })
    }
  }
  onChangeOrderStatus(id, status) {
    swal.fire({
      title: 'Are you sure?',
      text: "Do you want to change current status to " + status + "  for this order",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Change it'
    }).then((result) => {
      if (result.value) {
        this.updateOrderStatus(id, status);
      } else {
        this.getAllOrders();
      }
    });
  }
  onChangeOrderStatusForReport(status) {
    this.orderReportForm.controls['status'].setValue(status);
  }
  activeDeactiveUser(id, email, status) {

    if (this.user.email === email) {
      // this.messageService.addError("Ooops...! ");
      swal.fire({
        icon: 'error',
        title: 'Oops...! This is you..',
        text: 'I think you have recheck your action'
      })
    } else {
      swal.fire({
        title: 'Are you sure?',
        text: "Do you want to " + status + " this user",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, ' + status + ' please..!'
      }).then((result) => {
        if (result.value) {
          this.activeDeactiveUserWithConfirm(id, email, status);
        }
      })
    }
  }
  updateOrderStatus(id, status) {
    if (id) {
      let obj = { payment_status: status };
      this._InitService.updateOrderById(obj, id).subscribe(orderData => {
        if (orderData.data) {
          swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Successfully change to ' + status + ' status',
            showConfirmButton: false,
            timer: 1500
          })
          this.getAllOrders();

        } else {
          console.log("error getting order..");
        }
      });
    }

  }
  activeDeactiveUserWithConfirm(id, email, status) {
    if (id) {
      let obj = { id: id, active: status === 'activate' ? true : false };
      this._InitService.updateUserById(obj, id).subscribe(userData => {
        if (userData.data) {
          swal.fire({
            position: 'top-end',
            icon: 'success',
            title: email + ':  User ' + status + 'd successfully',
            showConfirmButton: false,
            timer: 1500
          })
          this.getAllUsers();

        } else {
          console.log("error getting users..");
        }
      });
    }

  }
  deleteOrderWithConfirm(id) {
    if (id) {
      this._InitService.removeOrder(id).subscribe(orderData => {
        if (orderData.data) {
          swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Order successfully removed',
            showConfirmButton: false,
            timer: 1500
          })
          this.getAllOrders();

        } else {
          console.log("error getting orders..");
        }
      });


    }

  }
  deleteUserWithConfirm(id, email) {
    if (id) {
      if (this.user.email === email) {
        // this.messageService.addError("Ooops...! ");
        swal.fire({
          icon: 'error',
          title: 'Oops...! This is you..',
          text: 'I think you have recheck your action'
        })
      } else {
        this._InitService.removeUser(id).subscribe(userData => {
          if (userData.data) {
            swal.fire({
              position: 'top-end',
              icon: 'success',
              title: email + ':  User successfully removed',
              showConfirmButton: false,
              timer: 1500
            })
            this.getAllUsers();

          } else {
            console.log("error getting users..");
          }
        });
      }

    }

  }
  getAllUsers() {
    this._InitService.getAllUsers().subscribe(userData => {
      if (userData.data) {
        this.allUsers = userData.data;
        this.searchTextUser = '';
      } else {
        console.log("error getting users..");
      }
    });
  }
  getColor(sku) {
    if (sku <= 5 && sku !== 0) {
      return '#fdbc45'
    } else if (sku > 5) {
      return '#60e460'
    } else if (sku <= 0) {
      return '#ef4545'
    }
  }
  getAllOrders() {
    this._InitService.getAllOrders().subscribe(orderData => {
      if (orderData.data) {
        this.allOrders = orderData.data;
        this.allOrders.forEach(orderElement => {
          var date = new Date(orderElement.created_on_date);
          // add days
          orderElement['returnDate'] = date.setDate(date.getDate() + parseInt(orderElement.delivery_date.toString().split(' ')[0]));
        });
        this.searchTextUser = '';
      } else {
        console.log("error getting orders..");
      }
    });
  }
  fileChoose(event) {
    console.log(event.target.files);
    if (event.target.files.length > 0) {
      this.isUploadDisable = false;
    }
  }
  fileRemoveAction(url) {
    swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        if (this.mode === 'edit') {
          this.productSubscription.unsubscribe();
          this.onFileRemove(url.toString().split('read?image=').length > 0 ? (url.toString().split('read?image=')[1]) : 'image', url);
          this.warning = true;
        } else {
          this.log.addError(`Cannot delete the image`);
        }
      }
    });
  }
  onFileRemove(imageName, url) {
    this._InitService.removeFile({ image_name: imageName }).subscribe(data => {
      if (data && data.status) {
        const index = this.product.image_urls.indexOf(url);
        if (index > -1) {
          this.product.image_urls.splice(index, 1);
          this.imageUrls.splice(this.imageUrls.indexOf(url), 1);
          console.log('Deleted.........!!');
          console.log(this.product.image_urls);
          console.log(this.imageUrls);
        }

      } else {
        console.log('DATA NOT FOUND');
      }
    });
  }
  private initForm() {
    this.productReportForm = new FormGroup({
      start_date: new FormControl(null,
        Validators.required
      ),
      end_date: new FormControl(null,
        Validators.required
      ),
    });
    this.orderReportForm = new FormGroup({
      start_date: new FormControl(null,
        Validators.required
      ),
      end_date: new FormControl(null,
        Validators.required
      ),
      status: new FormControl(null,
        Validators.required
      ),
    });
    this.orderReportForm.controls['status'].setValue("ALL");
    this.userReportForm = new FormGroup({
      start_date: new FormControl(null,
        Validators.required
      ),
      end_date: new FormControl(null,
        Validators.required
      ),
    });

    this.productForm = new FormGroup({
      name: new FormControl(
        this.product && this.product.name,
        Validators.required
      ),
      id: new FormControl(
        {
          value: this.product && this.product.id
        },
        Validators.required
      ),
      date: new FormControl(
        this.product && this.product.date,
        Validators.required
      ),
      categories: new FormControl(
        this.product && this.product.categories,
        Validators.required
      ),
      description: new FormControl(
        this.product && this.product.description,
        Validators.required
      ),
      actual_price: new FormControl(this.product && this.product.actual_price, [
        Validators.required,
        Validators.min(0)
      ]),
      original_price: new FormControl(this.product && this.product.original_price, [
        Validators.required,
        Validators.min(0)
      ]),
      stock_count: new FormControl(this.product && this.product.stock_count, [
        Validators.required,
        Validators.min(1)
      ])
    });
    this.onFormChanges();
  }
  changeTabIndex(index) {
    if (index === 0) {
      this.product = new DomainProduct();
      console.log(this.product);
      this.initForm();
      this.router.navigate(['/admin/add']);
    }
    this.activeTab = index;
    this.isDisabled(index === 1 ? true : false);

  }
  createReport() {
    let st = this.productReportForm.get('start_date').value;
    let ed = this.productReportForm.get('end_date').value;
    if (st && ed) {
      this._InitService.createReport({ start: st, end: ed }).subscribe(data => {
        console.log(data);
        if(data){
          window.open('http://68.183.22.7:80/report/read', "_blank");
        }
      });
    }else{
      this.log.addError('Please check your "Start Date" and "End Date"');
    }


  }
  createReportOrder() {
    let st = this.orderReportForm.get('start_date').value;
    let ed = this.orderReportForm.get('end_date').value;
    let status = this.orderReportForm.get('status').value;
    if (st && ed) {
      this._InitService.createReportOrder({ start: st, end: ed ,status:status}).subscribe(data => {
        console.log(data);
        if(data){
          window.open('http://68.183.22.7:80/order/report/read', "_blank");
        }
      });
    }else{
      this.log.addError('Please check your "Start Date" and "End Date"');
    }

  }
  createReportUser() {
    let st = this.userReportForm.get('start_date').value;
    let ed = this.userReportForm.get('end_date').value;
    if (st && ed) {
      this._InitService.createReportUser({ start: st, end: ed }).subscribe(data => {
        console.log(data);
        if(data){
          window.open('http://68.183.22.7:80/user/report/read', "_blank");
        }
      });
    }else{
      this.log.addError('Please check your "Start Date" and "End Date"');
    }


  }
  createUserWiseOrderReport() {
    let st = this.userReportForm.get('start_date').value;
    let ed = this.userReportForm.get('end_date').value;
    if (st && ed) {
      this._InitService.createUserWiseOrderReport({ start: st, end: ed }).subscribe(data => {
        console.log(data);
        if(data){
          window.open('http://68.183.22.7:80/order/report/by/user/read', "_blank");
        }
      });
    }else{
      this.log.addError('Please check your "Start Date" and "End Date"');
    }


  }
  resetProduct() {
    this.product.id = 0;
    this.product.name = '';
    this.product.date = '';
    this.product.currentRating = 0;
    this.product.description = '';
    this.product.imageFeaturedUrl = '';
    this.product.image_refs = [];
    this.product.image_urls = [];
    this.product.original_price = 0;
    this.product.ratings = {};
    this.product.reduction = 0;
    this.product.sale = false;
    this.product.actual_price = 0;
    this.product.categories = 'example, category';
  }
  isDisabled(value: boolean) {
    this._isDisabled = value;
    if (!this._isDisabled) {
      this.productForm.controls['id'].disable();
    } else {
      this.productForm.controls['id'].enable();
    }
  }
  private setProduct() {
    this.route.params.subscribe((params: Params) => {
      this.id = +this.route.snapshot.paramMap.get('id');
      // if we have an id, we're in edit mode
      console.log('mode **********************************************');
      console.log(this.id);
      if (this.id) {
        this.activeTab = 1;
        console.log('mode edit**********************************************123');
        this.mode = 'edit';
        this.isEditDisable = false;
        this.getProduct(this.id, true);
        this.initForm();
        console.log('this.product');
        console.log(this.product);
        console.log('this.product');
      } else {
        // else we are in add mode
        this.mode = 'add';
        this.activeTab = 0;
        this.isEditDisable = true;
        this.constructProduct(false);
        this.initForm();
      }
      this.productForm.controls['id'].disable();
    });
  }
  deleteItem(productItem) {
    swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.deleteProduct(productItem);
      }
    })

    console.log('productItem.id.value %%%%%%%%%%%%5 delete');
    console.log(productItem.id);

  }
  showImage(url) {
    swal.fire({
      imageUrl: url,
      imageHeight: 300,
      imageWidth: 300,
      imageAlt: 'A tall image'
    })
  }
  editItem(productItem) {
    this.isEditDisable = false;
    this.activeTab = 1;
    // console.log('productItem');
    console.log('productItem.id.value %%%%%%%%%%%%5');
    console.log(productItem.id);
    // // this.product = productItem;
    // this.getProduct(productItem.id,true);
    // this.customId = productItem.id;
    this.router.navigate(['/admin/edit/' + productItem.id]);
    this.changeTabIndex(1);

    // this.product = productItem;

    // this.onC();
  }
  onUpdate() {
    console.log('onUpdate');
    console.log(this.product);
    console.log('onUpdate');
    // this.mode = 'edit';
    // this.onSubmit();
    let productToUpdate = this.constructProductToSubmit(this.product);
    if (productToUpdate.id && productToUpdate.id['value']) {
      productToUpdate.id = productToUpdate.id['value'];
    }
    this.updateProduct(productToUpdate);
  }
  private constructProduct(isFromEdit) {
    const product = this.constructMockProduct();
    product.categories = this.categoriesFromObjectToString(product.categories);
    console.log('product in contstsss');
    console.log(product);
    this.syncProduct(product, isFromEdit);
    this.initForm();
  }
  updateHighlight(event, type, product) {
    console.log(event.target.checked);
    console.log('event');
    console.log('event');
    let object = {};
    object[type] = event.target.checked;
    this._InitService.updateHighlightProductById(product.id, object).subscribe(
      (response: any) => {
        console.log(response);
        // this.router.navigate(['/products/' + response.id]);
      },
      (error) => this.log.addError('Could not update your product')
    );
  }
  getAllHighlightProducts() {
    // console.log('**********************getAllHighlightProducts');
    this.productSubscription = this._InitService
      .getAllHighlightProducts()
      .subscribe((products: any) => {
        console.log('**********************getAllHighlightProducts');
        console.log(products.data);

        this.highlightProducts = products.data;
        this.highlightProducts.forEach(highlightElement => {
          this.allProducts.forEach(element => {
            if (element.id === highlightElement.id) {
              console.log('found @@@@@@@@@@@@@@@@@@@@@@@222');
              element['is_new_arrivals'] = highlightElement.is_new_arrivals;
              element['is_on_sale'] = highlightElement.is_on_sale;
              element['is_best_rated'] = highlightElement.is_best_rated;
              element['is_featured'] = highlightElement.is_featured;
            }
          });
        });
        console.log('**********************getAllHighlightProducts after allProducts');
        console.log(this.allProducts);

      });
  }
  private getAllProducts(): void {
    this.productSubscription = this._InitService
      .getAllProducts()
      .subscribe((products: any) => {
        console.log('**********************products');
        console.log(products);
        if (products && products.data && products.data.length > 0) {
          this.allProducts = products.data;
          this.allProducts.forEach(productElemet => {
            productElemet.categories = this.categoriesFromObjectToString(
              productElemet.categories
            );
            this.syncProduct(productElemet, false);
          });
          console.log('this.allProducts');
          console.log(this.allProducts);
          this.getAllHighlightProducts();
        }
      });
  }
  private getProduct(id, isIdActive): void {
    let paramMap = new Map();
    paramMap.set('id', id.toString().trim());
    this.productSubscription = this._InitService
      .getProductById(paramMap)
      .subscribe((product) => {
        console.log(product.data[0]);
        if (product && product.data && product.data.length > 0) {
          product.data[0].categories = this.categoriesFromObjectToString(
            product.data[0].categories
          );
          this.product = product.data[0];
          console.log(product.data[0]);
          this.syncProduct(this.product, true);

          this.initForm();

        }
      });
  }

  private onFormChanges() {
    this.formSubscription = this.productForm.valueChanges.subscribe(
      (formFieldValues) => {
        const product = { ...this.product, ...formFieldValues };
        this.syncProduct(product, true);
      }
    );
  }

  private syncProduct(product, isFromEdit?): void {
    let id = product.id;
    console.log(id);
    console.log('id*******88');
    const image_urls = this.handleImageURLs(product);
    const image_resize_urls = this.handleImageResizeURLs(product);
    const reduction = this.calculateReduction(
      product.original_price,
      product.actual_price
    );
    const sale = this.checkForSale(reduction);
    if (isFromEdit) {
      this.product = {
        ...product,
        sale,
        reduction,
        id,
        image_urls,
        image_resize_urls
      };
    }

    console.log("product");
    console.log(product);
  }

  onC() {
    if (this.document.getElementById('exampleModal')) {
      this.document.body.append(document.getElementById('exampleModal'));
    }
  }
  public onSubmit(isFromTable?) {
    console.log(this.productForm.get('description').value);
    // this.syncProduct({ ...this.product, ...this.productForm.value });
    const product = this.constructMockProduct();
    this.syncProduct(this.product, true);
    this.product.id = this.createId(this.product);
    this.product.stock_count = this.productForm.get('stock_count').value;
    const productToSubmit = this.constructProductToSubmit(this.product);
    console.log('this.product.id constructProductToAdd************* ---> ' + JSON.stringify(this.constructProductToAdd(productToSubmit)));
    console.log(productToSubmit);
    // const files: FileList = this.photos.nativeElement.files;
    // if (this.mode === 'add' && files.length > 0) {
    if (this.mode === 'add') {
      productToSubmit['is_new_arrivals'] = false;
      productToSubmit['is_on_sale'] = false;
      productToSubmit['is_best_rated'] = false;
      productToSubmit['is_featured'] = false;
      this.addProduct(productToSubmit);
    } else if (this.mode === 'edit') {

    } else {
      this.log.addError('Please provide a file for your product');
      return;
    }
  }

  private addProduct(product: any) {
    this._InitService.createProduct(product).subscribe(
      (savedProduct: any) => {
        this.messageService.add('Product Creation Successfull..!!');
        this.warning = false;
        this.getAllProducts();
        if (savedProduct.id) {
          // this.product = null;
          // this.router.navigate(['/products']);
        }
      },
      (error) => {
        this.log.addError('Could not upload your product');
        return of(error);
      }
    );
  }

  private deleteProduct(product, files?: FileList) {
    this.productSubscription.unsubscribe();
    // product.id = product.id.value;
    this._InitService.removeProduct(product.id).subscribe(
      (response: any) => {
        this.messageService.add('Product Removed Successfully..!!');
        this.getAllProducts();
        this.searchText = '';
        // this.router.navigate(['/products/' + response.id]);
      },
      (error) => this.log.addError('Could not delete your product')
    );
  }
  private updateProduct(product, files?: FileList) {
    this.productSubscription.unsubscribe();
    this._InitService.updateProductById(product).subscribe(
      (response: any) => {
        this.messageService.add('Product Updated Successfully..!!');
        this.warning = false;
        this.getAllProducts();
        // this.router.navigate(['/products/' + response.id]);
      },
      (error) => this.log.addError('Could not update your product')
    );
  }

  public onDelete() {
    swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        if (this.mode === 'edit') {
          this.productSubscription.unsubscribe();
          this.deleteProduct(this.product);
        } else {
          this.log.addError(`Cannot delete new product`);
        }
      }
    })

  }

  // pure helper functions start here:
  private constructMockProduct() {
    return new Product();
  }
  private constructProductToAdd(product: Product) {
    let tempProduct = new Product();
    if (!product.ratings) {
      product.ratings = tempProduct.ratings;
    }
    if (!product.currentRating) {
      product.currentRating = tempProduct.currentRating;
    }
    if (!product.sale) {
      product.sale = tempProduct.sale;
    }

    return product;
  }
  private constructProductFromForm() {
    let tempProduct = new Product();
    tempProduct.name = this.productForm.get('name').value;
    tempProduct.id = this.productForm.get('id').value;
    tempProduct.date = this.productForm.get('date').value;
    tempProduct.categories = this.productForm.get('categories').value;
    tempProduct.description = this.productForm.get('description').value;
    tempProduct.image_urls = this.productForm.get('description').value;
    tempProduct.name = this.productForm.get('description').value;
    tempProduct.name = this.productForm.get('description').value;
    tempProduct.name = this.productForm.get('description').value;
  }

  private constructProductToSubmit(product: DomainProduct): Product {
    return {
      ...product,
      categories: this.categoriesFromStringToObject(product.categories)
    };
  }

  private createId(product: Product): number {
    console.log('id created');
    const randomId = Math.floor(Math.random() * new Date().getTime());
    let id = randomId;
    if (id === 1) {
      id = Math.floor(Math.random() * new Date().getTime());
    }
    return id;
  }

  private categoriesFromObjectToString(categories: {}): string | null {
    if (Object.keys(categories).length === 0) {
      return 'example, category';
    }
    return Object.keys(categories).reduce(
      (result, currentProduct, index, inputArray) => {
        if (index < inputArray.length - 1) {
          return result + currentProduct + ',';
        }
        return result + currentProduct;
      },
      ''
    );
  }

  private categoriesFromStringToObject(categories: string): {} {
    // categories: 'cat1, cat2, cat3' || ''
    if (categories.length === 0) {
      return {};
    }
    return categories
      .split(',')
      .reduce((combinedCategories, currentCategory) => {
        combinedCategories[currentCategory.trim()] = true;
        return combinedCategories;
      }, {});
  }

  private checkForSale(reduction: number): boolean {
    return reduction > 0;
  }

  private calculateReduction(original_price: number, actual_price: number): number {
    const reduction = Math.round((original_price - actual_price) / original_price * 100);
    return reduction > 0 ? reduction : 0;
  }

  private handleImageURLs(product: Product): string[] {
    if (product.image_urls && product.image_urls.length > 0) {
      return product.image_urls;
    }
    return [];
  }
  private handleImageResizeURLs(product: Product): string[] {
    if (product.image_resize_urls && product.image_resize_urls.length > 0) {
      return product.image_resize_urls;
    }
    return [];
  }

  ngOnDestroy() {
    this.formSubscription.unsubscribe();
  }
}
