import { NgModule } from '@angular/core';
import { AddEditComponent } from './add-edit/add-edit.component';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProductsModule } from '../products/products.module';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { InitService } from './add-edit/services/init/init.service';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { FilterPipe } from '../filter.pipe';
import { FileUploadModule } from 'ng2-file-upload';
import { AngularCollapseModule } from 'angular-collapse';
import { NgxPaginationModule } from 'ngx-pagination';
import { FilterUserPipe } from '../filterUser.pipe';
import { FilterOrderPipe } from '../filterOrders.pipe';
@NgModule({
    declarations: [
        AddEditComponent,
        FilterPipe,
        FilterUserPipe,
        FilterOrderPipe
    ],
    imports: [
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        ProductsModule,
        AngularEditorModule,
        AngularFontAwesomeModule,
        FileUploadModule,
        AngularCollapseModule,
        NgxPaginationModule
    ],
    exports: [
        SharedModule,
        ProductsModule
    ],
    providers: [InitService]
})
export class AdminModule {}
