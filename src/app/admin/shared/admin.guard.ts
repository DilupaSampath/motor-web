import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthServiceUser } from '../../account/shared/auth.service';

import { take ,  map ,  tap } from 'rxjs/operators';

@Injectable()
export class AdminGuard implements CanActivate {
  constructor(private authService: AuthServiceUser, private router: Router) {}

  public canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
let authorized =false;
    let user = this.authService.getUser(); 
    if(user && user.role === 'admin' ? true : false){
        this.router.navigate(['/register-login']);
        return true;
    }else{
      return false;
    }
    
  }
}
