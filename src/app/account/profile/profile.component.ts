import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, Validators, FormControl, FormBuilder } from '@angular/forms';

import { AuthServiceUser } from '../shared/auth.service';

import { User } from '../../models/user.model';
import { Subscription } from 'rxjs';
import { InitService } from '../register-login/services/init/init.service';
import { MessageService } from '../../messages/message.service';
import { FileUploader } from 'ng2-file-upload';
import { environment } from '../../../environments/environment';
const URL = environment.api_url + 'api/upload';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {
  private authSubscription: Subscription;
  public formProfile: FormGroup;
  public profileErrors: string;
  private user: any;
  public uploader: FileUploader = new FileUploader({ url: URL, itemAlias: 'photo' });
  warning = false;
  constructor(private authService: AuthServiceUser, 
    private _InitService :InitService,
    private log: MessageService,
    private messageService: MessageService,
    private formBuilder: FormBuilder  ) { }

  ngOnInit() {

    this.initFormGroup();
    if(this.authService.getUser()){
      console.log('this.authenticationService.getUser() *******************');
      this.user = this.authService.getUser();
      this.setCurrentUser();
      console.log('this.authenticationService.getUser()');
    }else{
      this.authService.getUserByApiId(sessionStorage.getItem('userId')).subscribe(data2=>{
        console.log('not getting user get this');
        console.log(data2.data);
        this.authService.setUser(data2.data);
        this.user = this.authService.getUser();
        this.setCurrentUser();
      });

    }
  }
  setCurrentUser(){
    this.formProfile.controls['firstName'].setValue(this.user.firstName);
    this.formProfile.controls['lastName'].setValue(this.user.lastName);
    this.formProfile.controls['email'].setValue(this.user.email);

  }
  mustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];
        const matchingControl = formGroup.controls[matchingControlName];

        if (matchingControl.errors && !matchingControl.errors.mustMatch) {
            // return if another validator has already found an error on the matchingControl
            return;
        }

        // set error on matchingControl if validation fails
        if (control.value !== matchingControl.value) {
            matchingControl.setErrors({ mustMatch: true });
        } else {
            matchingControl.setErrors(null);
        }
    }
}
  setChagersToUser(){
    this.user.firstName =  this.formProfile.controls['firstName'].value;
    this.user.lastName = this.formProfile.controls['lastName'].value;
    this.user.email = this.formProfile.controls['email'].value;
    if(this.authService.uploded_url){
      this.user.photoURL = this.authService.uploded_url;
    }
  }
  
  private initFormGroup() {
    this.formProfile = this.formBuilder.group({
      firstName: new FormControl(null, Validators.required),
      lastName: new FormControl(null, Validators.required),
      email: new FormControl(null, Validators.email),
      password: new FormControl(null),
      confirmPassword: new FormControl(null),
    }, {
      validator: this.mustMatch('password', 'confirmPassword')
  });
  }
updateUser(){
  // this.authService.updateProfile(this.user);
  this.setChagersToUser();
  let user ={};
  user['email'] =  this.formProfile.get('email').value;
  user['first_name'] =  this.formProfile.get('firstName').value;
  user['last_name'] =  this.formProfile.get('lastName').value;
  user['password'] =  this.formProfile.get('password').value;

  this._InitService.updateUser(this.user.uid, user).subscribe((responce: any) => {
    if (responce.status) {
      this.messageService.add('User profile has been updated!');
      this.authService.uploded_url = null;
    } else {
      this.log.addError('Could not upload your product');
    }
  });
}
  public onSubmit() {

    // Update Email
    if (this.user.email !== this.formProfile.value.email) {
      this.authService.updateEmail(this.formProfile.value.email)
      .catch(
        error => {
          this.profileErrors = error.message;
          this.formProfile.patchValue({ email: this.user.email });
        }
      );
    }

    // Update Profile (Firstname, Lastname)
    if (this.user.firstName !== this.formProfile.value.firstName || this.user.lastName !== this.formProfile.value.lastName) {
      this.authService.updateProfile(this.formProfile.value);
    }

    // Update password
    if (this.formProfile.value.password && this.formProfile.value.confirmPassword
      && (this.formProfile.value.password === this.formProfile.value.confirmPassword)) {
      this.authService.updatePassword(this.formProfile.value.password)
      .catch(
        error => {
          this.profileErrors = error.message;
        }
      );
    }
  }

  ngOnDestroy() {
    // this.authSubscription.unsubscribe();
  }
}
