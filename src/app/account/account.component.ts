import { Component, OnInit } from '@angular/core';

import { AuthServiceUser } from './shared/auth.service';
import { Router } from '@angular/router';
import { OrderService } from './orders/shared/order.service';

import { User } from '../models/user.model';
import { FileUploader } from 'ng2-file-upload';
import { environment } from '../../environments/environment';
import { MessageService } from '../messages/message.service';
import { InitService } from './register-login/services/init/init.service';
const URL = environment.api_url + 'api/upload';


@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit{
  public user: any;
  public uploader: FileUploader = new FileUploader({ url: URL, itemAlias: 'photo' });
  onUploaded = false;
  warning = false;
  progress = 20;
  isProgressbarActive= false;
  isUploadError = false;
  constructor(
    private authService: AuthServiceUser,
    public router: Router,
    private messageService: MessageService,
    private log: MessageService,
    public orderService: OrderService,
    private _InitService :InitService
  ) {}

  ngOnInit(){
    this.authService.getUserByApiId(sessionStorage.getItem('userId')).subscribe(data2=>{
      console.log('not getting user get this');
      console.log(data2.data);
      this.authService.setUser(data2.data);
      this.user = this.authService.getUser(); 
    });
    this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
    this.uploader.onErrorItem = (item, response, status, headers) => {
      this.progress = 50;
      this.isProgressbarActive =true;
      this.isUploadError = true;
      this.log.addError('Fail to uploaded this file..!');
      setTimeout(() => {
        this.isProgressbarActive =false;
        this.progress = 20
        this.isUploadError = false;
      }, 3000);
      
    };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      //  console.log('ImageUpload:uploaded:', item, status, response);

      console.log(JSON.parse(response).resize_file_path);
      //  console.log(item);
      //  alert('File uploaded successfully');
      this.authService.getUserByApiId(sessionStorage.getItem('userId')).subscribe(data2=>{
        console.log('not getting user get this');
        console.log(data2.data);
        this.authService.setUser(data2.data);
        this.user = this.authService.getUser(); 
      });
      if(JSON.parse(response).resize_file_path){
        this.authService.uploded_url = (JSON.parse(response).resize_file_path);
      }else{
        this.authService.uploded_url = (JSON.parse(response).image_urls);
      }

      this.onUploaded = true;
      this.progress = 100;
      this.isProgressbarActive =true;
      this.isUploadError = false;
      setTimeout(() => {
        this.isProgressbarActive =false;
        this.progress = 20;
        this.isUploadError = false;
      }, 3000);
      console.log(this.user);
      this._InitService.updateUser(this.user.uid,{"profile_picture":this.authService.uploded_url}).subscribe(data=>{
        console.log(data);
        if(data.status){
          this._InitService.getUser(this.user.uid).subscribe(userData=>{
            if(userData.status && userData.data.profile_picture){
              this.authService.uploded_url = userData.data.profile_picture;
              this.user.photoURL = userData.data.profile_picture;
            }
          });
          this.messageService.add('Profile picture updated successfully..!!');
        }else{
          this.log.add('Fail to update your profile picture..!!');
        }
      });

      this.warning = true;
      setTimeout(() => {
        this.isProgressbarActive =false;
        this.progress = 20
        this.isUploadError = false;
      }, 3000);
      setTimeout(() => {
        this.warning = false;
      }, 5000);
      console.log(this.user.profile_picture);
      this.warning = false;
      this.onUploaded = false;
  
    };  

  }
  uploadEvent(){
    this.isProgressbarActive =true;
    this.progress = 50;
  }
  fileChoose(event){
    console.log(event.target.files);
    this.warning = true;
    this.onUploaded = true;
    // if (event.target.files.length > 0) {
    //     this.warning = true;
    // }
  }

}
