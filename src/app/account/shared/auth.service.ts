import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

import { Observable, of } from 'rxjs';
import { take, takeUntil, switchMap, map } from 'rxjs/operators';

import { MessageService } from '../../messages/message.service';
import { User, Roles } from '../../models/user.model';
import { InitService } from '../register-login/services/init/init.service';
import { MainService } from '../../infrastructure/api.service';

@Injectable()
export class AuthServiceUser {
  public user: any;
  public uploded_url: string;
  constructor(
    private afAuth: AngularFireAuth,
    private db: AngularFireDatabase,
    private messageService: MessageService,
    private _InitService: InitService,
    private _apiService: MainService,
    private log: MessageService,
  ) {
    // this.afAuth.authState
    //   .pipe(
    //     switchMap((auth) => {
    //       if (auth) {
    //         return this.db.object('users/' + auth.uid).valueChanges()
    //           .pipe(
    //             map(user => {
    //               return {
    //                 ...user,
    //                 uid: auth.uid
    //               };
    //             })
    //           );
    //       } else {
    //         return of(null);
    //       }
    //     })
    //   );
  }
  public setUser(userNew) {
    let user = {};
    user['email'] = userNew.email;
    user['firstName'] = userNew.first_name;
    user['lastName'] = userNew.last_name;
    user['orders'] = userNew.orders;
    user['photoURL'] = userNew.profile_picture;
    user['role'] = userNew.role;
    user['uid'] = userNew._id;
    this.user = user;

  }
  public getUser() {
    return this.user;
  }
  public googleLogin() {
    const provider = new firebase.auth.GoogleAuthProvider();
    return this.afAuth.auth.signInWithPopup(provider).then(
      (credential) => {
        this.updateNewUser(credential.user);
      },
      (error) => {
        throw error;
      }
    );
  }
  getUserByApiId(id): Observable<any> {
    return this._apiService.get(`user/get/` + id);
  }

  public emailSignUp(email: string, password: string) {
    return this.afAuth.auth
      .createUserWithEmailAndPassword(email, password)
      .then(
        (user) => {
          this.updateNewUser(user);
        },
        (error) => {
          throw error;
        }
      );
  }

  emailLogin(email: string, password: string) {
    return this.afAuth.auth.signInWithEmailAndPassword(email, password).then(
      (user) => {
        this.updateNewUser(user);
      },
      (error) => {
        throw error;
      }
    );
  }

  public signOut() {
    localStorage.clear();
    this.setUser(new User({}));
    this.messageService.add('You have been logged out.');
  }

  public updateProfile(userData: User) {
    this.updateExistingUser(userData);
  }

  public updatePassword(password: string) {
    return this.afAuth.auth.currentUser
      .updatePassword(password)
      .then(() => {
        this.messageService.add('Password has been updated!');
      })
      .catch(function (error) {
        throw error;
      });
  }

  public updateEmail(email: string) {
    return this.afAuth.auth.currentUser
      .updateEmail(email)
      .then(() => {
        this.updateExistingUser({ email: email });
        this.messageService.add('User email have been updated!');
      })
      .catch(function (error) {
        throw error;
      });
  }

  private updateNewUser(authData) {
    const userData = new User(authData);
    const ref = this.db.object('users/' + authData.uid);
    ref
      .valueChanges()
      .pipe(
        take(1)
      )
      .subscribe((user) => {
        if (!user) {
          ref.update(userData);
        }
      });
  }

  private updateExistingUser(userData) {
    console.log(userData);
  }
}
