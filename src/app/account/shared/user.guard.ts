import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthServiceUser } from './auth.service';

import { take, tap, map } from 'rxjs/operators';

@Injectable()
export class UserGuard implements CanActivate {
  constructor(private authService: AuthServiceUser, private router: Router) { }

  public canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    sessionStorage.getItem('userId');
    try {
      this.authService.getUserByApiId(sessionStorage.getItem('userId')).subscribe(element => {
        if (element.data) {
          if (element.data.role === 'admin') {
            return true;
          } else {
            return false;
          }

        } else {
          return false;
        }
      });
    } catch (error) {
      return false;
    }


  }
}
