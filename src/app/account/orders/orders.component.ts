import { Component, OnInit, OnDestroy } from '@angular/core';
import {
  CommonModule,
  DatePipe,
  DecimalPipe,
  CurrencyPipe
} from '@angular/common';

import { Subscription } from 'rxjs';

import { OrderService } from './shared/order.service';

import { Order } from '../../models/order.model';
import { AuthServiceUser } from '../shared/auth.service';
import { InitService } from './shared/init.service';
import swal from 'sweetalert2';
import * as $ from 'jquery';
import { NgxSmartModalService } from 'ngx-smart-modal';
// declare var $: any;
@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit, OnDestroy {
  public orders: any[];
  private ordersSubscription: Subscription;
  user:any;
  currentOrder: any;
  isError = false;
  searchText: any;
  isNotLoading = false;
  constructor(public ngxSmartModalService: NgxSmartModalService,public orderService: OrderService,    private authService: AuthServiceUser,    private _InitService: InitService) {}

  ngOnInit() {
    this.isNotLoading = false;
    this.authService.getUserByApiId(sessionStorage.getItem('userId')).subscribe(data2=>{
      console.log('not getting user get this');
      console.log(data2.data);
      this.authService.setUser(data2.data);
      this.user = this.authService.getUser(); 
      let paramMap = new Map();
      paramMap.set('uid', this.user.uid.toString().trim());
      this._InitService.getOrdersByCondition(paramMap).subscribe(resData=>{
        console.log(resData);
        this.isNotLoading = true;
        if(resData.data){
          this.orders = resData.data;
        }

      },(errorIn:any)=>{
        this.isError = true;
        this.isNotLoading = false;
      });
    },(errorOut:any)=>{
      this.isError = true;
      this.isNotLoading = false;
    });

  }

  setCurrentOrder(order){
    this.currentOrder = order;
  }  
  ngOnDestroy(){
  }
}
