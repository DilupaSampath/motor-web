import { Injectable } from '@angular/core';

// import common service
import { MainService } from '../../../infrastructure/api.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InitService {
  constructor(private _apiService: MainService) { }
  loggedUser: string = window.sessionStorage.getItem('userId');

  getAllOrders(): Observable<any> {
    return this._apiService.get(`orders/getAll/`);
  }
  getOrdersByUserId(prmsId) {
    return this._apiService.get(`orders/get/` + prmsId);
  }
  getOrdersByCondition(paramsMap: Map<string, string>) {
    return this._apiService.get(`orders/getAll`, paramsMap);
  }
  updateOrderById(obj): Observable<any> {
    return this._apiService.patch(`orders/update/` + obj.id, obj);
  }
  createOrder(obj): Observable<any> {
    return this._apiService.post(`order/new`, obj);
  }
  // postObjArrvalTime(obj): Observable<any> {
  //   return this._apiService.post(`googleApi/getDataWithArrivelTime`, obj);
  // }
  // postObjNearByPlaceMapRequest(obj): Observable<any> {
  //   return this._apiService.post(`googleApi/getNearByPlaces`, obj);
  // }

  // validatePoint(obj): Observable<any> {
  //   return this._apiService.post(`googleApi/validatePoint`, obj);
  // }

  removeProduct(id): Observable<any> {
    return this._apiService.delete(`product/remove/${id}`);
  }
}
