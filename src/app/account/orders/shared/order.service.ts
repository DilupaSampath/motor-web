import { Injectable, OnInit } from '@angular/core';
import { Observable ,  of ,  from as fromPromise } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { AngularFireDatabase } from 'angularfire2/database';

import { Order } from '../../../models/order.model';

import { MessageService } from '../../../messages/message.service';
import { AuthServiceUser } from '../../shared/auth.service';
import { InitService } from './init.service';

@Injectable()
export class OrderService {
  user:any;
  constructor(
    private messageService: MessageService,
    private authService: AuthServiceUser,
    private store: AngularFireDatabase,
    private _InitService: InitService
  ) {
    this.authService.getUserByApiId(sessionStorage.getItem('userId')).subscribe(data2=>{
      console.log('not getting user get this');
      console.log(data2.data);
      this.authService.setUser(data2.data);
      this.user = this.authService.getUser(); 
    });
  }

  public getOrders() {
    this.authService.getUserByApiId(sessionStorage.getItem('userId')).subscribe(data2=>{
      console.log('not getting user get this');
      console.log(data2.data);
      this.authService.setUser(data2.data);
      this.user = this.authService.getUser(); 
      let paramMap = new Map();
      paramMap.set('uid', this.user.uid.toString().trim());
      this._InitService.getOrdersByCondition(paramMap).subscribe(resData=>{
        console.log(resData);
      });
    });

    // return this.authService.user
    //   .pipe(
    //     switchMap((user) => {
    //       if (user) {
    //         const remoteUserOrders = `/users/${user.uid}/orders`;
    //         return this.store.list(remoteUserOrders).valueChanges();
    //       } else {
    //         return of(null);
    //       }
    //     })
    //   );
  }

  public addUserOrder(order: Order, total: number, user: string) : Observable<any> {
    let requestBody ={};
    requestBody['products'] = [];
    requestBody['delivery_date'] = order.shippingDate >3 ? order.shippingDate + " Days":order.shippingDate * 24+' Hours';
    requestBody['created_on_date'] = new Date().getTime();
    requestBody['total_price'] = total;
    requestBody['payment_type'] = order.paymentMethod ==="Paypal"? 'CC':'COD';
    requestBody['order_address'] = order.customer.address1;
    requestBody['recipient_name'] = order.customer.firstname +' '+ order.customer.lastname;
    requestBody['contact_number'] = order.customer.phone+'';
    requestBody['delivery_cost'] = order.shippingCost;
    requestBody['status'] = "PENDING";
    requestBody['user_id'] = user;

    order.items.forEach(element=>{
      requestBody['products'].push( {"name":element.product.name,"qty":element.amount , "price":element.product.actual_price,id:element.product.id});
    });
    // const orderWithMetaData = {
    //   ...order,
    //   ...this.constructOrderMetaData(order),
    //   total
    // };
    return this._InitService.createOrder(requestBody);
    // const databaseOperation = this.store
    //   .list(`users/${user}/orders`)
    //   .push(orderWithMetaData)
    //   .then((response) => response, (error) => error);
// 
    // return fromPromise(databaseOperation);
  }

  public addAnonymousOrder(order: Order, total: number) {
    const orderWithMetaData = {
      ...order,
      ...this.constructOrderMetaData(order),
      total
    };

    const databaseOperation = this.store
      .list('orders')
      .push(orderWithMetaData)
      .then((response) => response, (error) => error);

    return fromPromise(databaseOperation);
  }

  private constructOrderMetaData(order: Order) {
    return {
      number: (Math.random() * 10000000000).toString().split('.')[0],
      date: new Date().toString(),
      status: 'In Progress'
    };
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.messageService.addError(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
