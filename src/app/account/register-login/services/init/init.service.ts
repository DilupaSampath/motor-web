import { Injectable } from '@angular/core';

// import common service
import { MainService } from '../../../../infrastructure/api.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InitService {
  constructor(private _apiService: MainService) {}
  loggedUser: string = window.sessionStorage.getItem('userId');

  getAllUsers(): Observable<any> {
    return this._apiService.get(`user/getAll`);
  }
  getUser(id): Observable<any> {
    return this._apiService.get(`user/get/`+id);
  }

  userLogin(obj): Observable<any> {
    return this._apiService.post(`user/login`, obj);
  }
  userCreate(obj): Observable<any> {
    return this._apiService.post(`user/new`, obj);
  }
  resetPasswordByUserName(username): Observable<any> {
    return this._apiService.get(`user/passowrd/reset/`+username);
  }

  // postObjArrvalTime(obj): Observable<any> {
  //   return this._apiService.post(`googleApi/getDataWithArrivelTime`, obj);
  // }
  // postObjNearByPlaceMapRequest(obj): Observable<any> {
  //   return this._apiService.post(`googleApi/getNearByPlaces`, obj);
  // }

  // validatePoint(obj): Observable<any> {
  //   return this._apiService.post(`googleApi/validatePoint`, obj);
  // }

  updateHighlightProductById(id,obj): Observable<any> {
    return this._apiService.patch(`product/highlight/update/`+id, obj);
  }

  updateUser(id,obj): Observable<any> {
    return this._apiService.patch(`user/update/${id}`,obj);
  }
}
