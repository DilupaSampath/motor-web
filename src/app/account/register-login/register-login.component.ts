import { Component, OnInit, ViewChild, OnChanges } from '@angular/core';
import {
  FormGroup,
  ReactiveFormsModule,
  FormControl,
  Validators
} from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MessageService } from '../../messages/message.service';
import { AuthServiceUser } from '../shared/auth.service';
import { InitService } from './services/init/init.service';
import { User } from '../../models/user.model';
import {
    AuthService,
    FacebookLoginProvider,
    GoogleLoginProvider,
    LinkedinLoginProvider
} from 'angular-6-social-login';
import swal from 'sweetalert2';
@Component({
  selector: 'app-register-login',
  templateUrl: './register-login.component.html',
  styleUrls: ['./register-login.component.scss']
})
export class RegisterLoginComponent implements OnInit {
  public loginForm: FormGroup;
  public registerForm: FormGroup;
  public registerErrors: string;
  signUp = false;
  constructor(
    private authenticationService: AuthServiceUser,
    private router: Router,
    private messageService: MessageService,
    private log: MessageService,
    private _InitService:InitService,
    private socialAuthService: AuthService
  ) {}

  public socialSignIn(socialPlatform : string) {
    let socialPlatformProvider;
    if(socialPlatform == "facebook"){
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    }else if(socialPlatform == "google"){
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    } else if (socialPlatform == "linkedin") {
      socialPlatformProvider = LinkedinLoginProvider.PROVIDER_ID;
    }
    console.log(' userData&&&&&&&&&&&&&&&&&&&&&***************881');
    // this.socialAuthService.authState.ini
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        if(userData.email){
          
        console.log(socialPlatform+" sign in data : " , userData);
        console.log( userData.email);
        console.log(' userData&&&&&&&&&&&&&&&&&&&&&***************88');
        // Now sign-in with userData
        // ...
          this._InitService.userCreate({auto_password : true, role:'user',email: userData.email, name: userData.name})
          .subscribe(data => {
            console.log(data.data);
            if(!data.data.isAlreadyExist){
          this.loginForm.setValue({ email: data.data.user.email, password: ''});
              this.messageService.add('Account created successfully. Your password is sent to your email address.!');
            }else{
              if(!data.data.foundError){
              this.loginForm.setValue({ email: data.data.user.email, password: ''});
              sessionStorage.setItem('userId',data.data.user._id);
              sessionStorage.setItem('role',data.data.user.role);
              delete data.data.user.createdAt;
              delete data.data.user.updatedAt;
              delete data.data.user.__v;
    
              if(data.data.user.id){
                this.authenticationService.setUser(data.data.user);
              }
    
              console.log('this.authenticationService.getUser()');
              console.log(this.authenticationService.getUser());
              console.log('this.authenticationService.getUser()');
              let timerInterval
    swal.fire({
      title: `Please wait i'm processing...`,
      timer: 2000,
      timerProgressBar: true,
      onBeforeOpen: () => {
        swal.showLoading()
        timerInterval = setInterval(() => {
          const content  =  swal.getContent() 
          if (content) {
            const b =  content.querySelector('b')as HTMLCanvasElement;  
            if (b) {
              b.textContent  =  swal.getTimerLeft().toString();
            }
          }
        }, 100)
      },
      onClose: () => {
        clearInterval(timerInterval)
      }
    }).then((result) => {
      /* Read more about handling dismissals below */
      if (result.dismiss === swal.DismissReason.timer) {
        this.router.navigate(['/home']);
      }
    })
  }else{
    this.log.addError(data.data.error); 
  }
            }
              // this.loginForm.setValue({ email: this.registerForm.value.email, password: ''});
              // this.initRegisterForm();
              // this.onSignIn();
            },(error) => {
              console.log(error);
              this.registerErrors = error.msg;
              // if (error.code === 'auth/weak-password') {
              //   this.registerForm.controls.password.setErrors({ password: true });
              //   this.registerForm.controls.confirmPassword.setErrors({ confirmPassword: true });
              // }
              // if (error.code === 'auth/email-already-in-use') {
              //   this.registerForm.controls.email.setErrors({ email: true });
              // }
            }
          );
        }else{

        }
            
      }
    );
  }

  keyDownFunction(event) {
    if(event.keyCode == 13) {
     console.log(event.keyCode);
     this.onLogin();
      // rest of your code
    }
    console.log(event.keyCode);
  }



  ngOnInit() {
    this.initLoginForm();
    
    this.initRegisterForm();
  }

  private initLoginForm() {
    this.loginForm = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, Validators.required)
    });
  }

  private initRegisterForm() {
    this.registerForm = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, Validators.required),
      confirmPassword: new FormControl(null, Validators.required)
    });
  }
  forgotPassword(){
    swal.mixin({
      input: 'text',
      confirmButtonText: 'Next &rarr;',
      showCancelButton: true,
      progressSteps: ['1', '2']
    }).queue([
      {
        title: 'Please enter your user name',
        text: 'Let me hlep you. First We need to  can find yor account'
      },
      {
        title: `Please type 'reset'`,
        text: `I'm going to reset your password..`
      } 
    ]).then((result) => {
      const answers = JSON.stringify(result.value);
      console.log(result.value[1]);
      console.log(result.value[0]);
      if (result.value && result.value.length > 1) {
        if(result.value[1].toString()=== 'reset'){
          this._InitService.resetPasswordByUserName(result.value[0])
          .subscribe(data => {
            console.log(data);
              // this.messageService.add('Account created successfully. Please login with your new credentials!');
              this.loginForm.setValue({ email: result.value[0], password: ''});
              this.initRegisterForm();
              this.onSignIn();
              swal.fire({
                position: 'top-end',
                icon: 'success',
                title: 'Please check your email and verify your account',
                showConfirmButton: false,
                timer: 2500
              })
            },(error) => {
              console.log(error);
              this.registerErrors = error.msg;
              // if(error.inValidEmail){

              // }
              if(error.msg.inValidEmail){
                swal.fire({
                  icon: 'error',
                  title: 'Oops...',
                  text: error.msg.msg,
                });

              }else{
                this.messageService.addError('Something went wrong. Please try again!');

              }
              // if (error.code === 'auth/weak-password') {
              //   this.registerForm.controls.password.setErrors({ password: true });
              //   this.registerForm.controls.confirmPassword.setErrors({ confirmPassword: true });
              // }
              // if (error.code === 'auth/email-already-in-use') {
              //   this.registerForm.controls.email.setErrors({ email: true });
              // }
            }
          );
        }else{
            swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: `That's not correct. Let's try again.`,
            });
        }

      }
    })
  }

  public onRegister() {
    if (this.registerForm.value.password !== this.registerForm.value.confirmPassword) {
      this.registerErrors = 'Passwords don\'t match!';
      this.registerForm.controls.password.setErrors({ password: true });
      this.registerForm.controls.confirmPassword.setErrors({ confirmPassword: true });
    } else {
      this._InitService.userCreate({auto_password : false,role:'admin',email: this.registerForm.value.email, password : this.registerForm.value.password})
      .subscribe(data => {
        console.log(data);
          this.messageService.add('Account created successfully. Please login with your new credentials!');
          this.loginForm.setValue({ email: this.registerForm.value.email, password: ''});
          this.initRegisterForm();
          this.onSignIn();
        },(error) => {
          console.log(error);
          this.registerErrors = error.msg;
          
          this.messageService.addError('Something went wrong. Please try again or use other options to login');
          // if (error.code === 'auth/weak-password') {
          //   this.registerForm.controls.password.setErrors({ password: true });
          //   this.registerForm.controls.confirmPassword.setErrors({ confirmPassword: true });
          // }
          // if (error.code === 'auth/email-already-in-use') {
          //   this.registerForm.controls.email.setErrors({ email: true });
          // }
        }
      );
    }
  }
  onSignUp(){
    this.signUp = true;
  }
  onSignIn(){
    this.signUp = false;
  }
  public onLogin() {
    this._InitService
      .userLogin({email:this.loginForm.value.email, password:this.loginForm.value.password})
      .subscribe(
        (data) => {
          if(!data.data.foundError){
            console.log(data);
            this.messageService.add('Login successful!');
            sessionStorage.setItem('userId',data.data._id);
            sessionStorage.setItem('role',data.data.role);
            delete data.data.createdAt;
            delete data.data.updatedAt;
            delete data.data.__v;
  
            if(data.data.id){
              this.authenticationService.setUser(data.data);
            }
  
            console.log('this.authenticationService.getUser()');
            console.log(this.authenticationService.getUser());
            console.log('this.authenticationService.getUser()');
  
            this.router.navigate(['/home']);
          }else{
            this.log.addError(data.data.error); 
          }
         

        },
        (error) => {
          console.log(error);
          console.log("error 8888888888888");
          this.log.addError('Invalid login. Please check your user name or passowrd');
          // this.loginForm.controls.email.setErrors({ email: true });
          // this.loginForm.controls.password.setErrors({ password: true });
          // if (error.code === 'auth/user-not-found') {
          //   this.loginForm.controls.email.setErrors({ email: true });
          // }
          // if (error.code === 'auth/wrong-password') {
          //   this.loginForm.controls.password.setErrors({ password: true });
          // }
        }
      );
  }
}
