export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyCXVTjTuBUXf-UWlpKfKHjomfPezutmPwI',
    authDomain: 'cas-fee-shop.firebaseapp.com',
    databaseURL: 'https://cas-fee-shop.firebaseio.com',
    projectId: 'cas-fee-shop',
    storageBucket: 'cas-fee-shop.appspot.com',
    messagingSenderId: '323643286137'
  },
  // api_url: 'http://localhost:3000/',
  api_url: 'http://68.183.22.7:80/',
  image__default_url: 'http://68.183.22.7/'

};
